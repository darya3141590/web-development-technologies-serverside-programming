<?php

interface Guide
{
    function howToFeed();
    function howToCare();

}

trait echoIntroduction
{
    public function introduction()
    {
        echo 'Я - ';
    }
}

trait echoDoing
{
    public function doing()
    {
        echo 'Временно занят - ';
    }
}

class Mammals{
    use echoIntroduction, echoDoing;

    private $name;
    private $animal;
    public $color;
    public $weight;
    protected $action;
    public $speed;
    public $gender;
    protected $sound;

    public function __construct($name, $gender, $color, $weight, $speed, $action, $sound, $animal)
    {
        $this->name = $name;
        $this->gender = $gender;
        $this->color = $color;
        $this->weight = $weight;
        $this->speed = $speed;
        $this->action = $action;
        $this->sound = $sound;
        $this->animal = $animal;
    }

    public function setName(string $name)
    {
        $this->name = $name;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setAnimal(string $animal)
    {
        $this->animal = $animal;
    }

    public function getAnimal(): string
    {
        return $this->animal;
    }
    public function getAction(): string
    {
        return $this->action;
    }

    public function getSound(): string
    {
        return $this->sound;
    }

    public function sayHello()
    {
        echo $this->sound . "! Привет! Меня зовут " . $this->name . ".";
    }
    // public function introduction()
    // {
    //     echo 'Я - ' . $this->animal;
    // }
    // public function doing()
    // {
    //     echo 'Временно занят - ' . $this->action;
    // }
}

class Dog extends Mammals implements Guide
{
    public $commands;
    public $count_toys;
    private $owner_name;
    public $food;

    public function __construct($name, $gender, $color, $weight, $speed, $action, $sound, $animal, $commands, $count_toys, $owner_name, $food)
    {
        parent::__construct($name, $gender, $color, $weight, $speed, $action, $sound, $animal);
        $this->commands = $commands;
        $this->count_toys = $count_toys;
        $this->owner_name = $owner_name;
        $this->food = $food;
    }

    public function setOwnerName(string $owner_name)
    {
        $this->owner_name = $owner_name;
    }

    public function getOwnerName(): string
    {
        return $this->owner_name;
    }

    public function talants()
    {
        echo 'Я знаю такие команды - ' . $this->commands;
    }
    public function howToFeed()
    {
        echo "Три раза в день сухим кормом '" . $this->food . "' по 336 грамм. Можно внедрить поощрение говяжьими трахеями.";
    }
    public function howToCare()
    {
        echo "Раз в месяц водить на грумминг, чтобы избавиться от лишнего подшерстка. Чистить уши чаще раза в месяц если собака аллергенна, стричь когти. Чистить зубы детской пастой.";
    }
    
}
$plactopus = new Mammals('Гарри', 'мужчина', 'коричнево-зеленый', 15, 2, 'ловлю рыбу', 'Прррр', 'утконос');
echo $plactopus->sayHello();
echo '<br>';
$plactopus->introduction();
echo $plactopus->color . ' ' . $plactopus->getAnimal() . '.';
echo '<br>';
echo 'Пол - ' . $plactopus->gender . '.';
echo '<br>';
$plactopus->setName('Снежок');
echo 'Мне сменили имя, теперь я - ' . $plactopus->getName() . '.';
echo '<br>';
echo 'Я вешу ' . $plactopus->weight . ' кг и плаваю со скоростью - ' . $plactopus->speed . ' км в час.';
echo '<br>';
echo $plactopus->doing() . $plactopus->getAction() . '.';
echo '<br><br>';

$labrador = new Dog('Лорд', 'мужчина', 'золотистый', 38, 12, 'охочусь на утку', 'Гав', 'лабрадор', 'нельзя, ждать, рядом, голос, нос, служить и ко мне', 25, 'Злата', 'Gemon');
echo $labrador->sayHello();
echo '<br>';
$labrador->introduction();
echo $labrador->color . ' ' . $labrador->getAnimal() . '.';
echo '<br>';
echo 'Пол - ' . $labrador->gender . '.';
echo '<br>';
echo 'Моя хозяйка - ' . $labrador->getOwnerName();
echo '<br>';
echo 'Я вешу ' . $labrador->weight . ' кг и бегаю со скоростью - ' . $labrador->speed . ' км в час.';
echo '<br>';
echo $labrador->doing() . $labrador->getAction() . '.';
echo '<br>';
$labrador->talants();
echo '<br>';
echo 'Инструкция по кормлению: ';
$labrador->howToFeed();
echo '<br>';
echo 'Инструкция по уходу: ';
$labrador->howToCare();