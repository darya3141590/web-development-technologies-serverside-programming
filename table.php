<?php
$cells = 10;
$rows = 10;
?>
123
<style>
table { border: 3px #840006 solid; border-collapse: collapse;}
td { padding: 2px 5px; width: 8%; text-align: center; border: 1px #840006 solid;}
</style>

<table>
<?php
for ($tr = 0; $tr <= $rows; $tr++)
{
    echo '<tr>';

    for ($td = 0; $td <= $cells; $td++)
    {
        $color = '';
        $border = 1;
        $font = 'normal';
        $text_color = 'black';

        if ($td === 0 or $tr === 0) {
                $color = '#ffcaa0';
                $border = 3;
                $font = 'bold';}
        if ($td === $tr) {
          if ($td === 0) {$color = '#840006'; $text_color = '#840006';}
          else {
            $color = '#faf69b';}
        }

        echo '<td style="background-color:', $color, '; border:', $border, 'px #840006 solid; font-weight:', $font, '; color:', $text_color, ';"">', $td?$tr?$tr*$td:$td:$tr, '</td>';
    }
    echo "</tr>";
}
?>
