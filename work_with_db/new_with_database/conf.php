<?php

spl_autoload_register();

use App\Database;
use App\Models\Subject;
use App\Models\Payment;
use App\Models\Participant;

if (getenv('REQUEST_METHOD') === 'POST')
{
    $data = [
        'userfirstname' => '',
        'userlastname' => '',
        'email' => '',
        'phone' => '',
        'topic' => '',
        'payment' => '',
        'mailing' => '',
    ];

    $errors = [];

    if (!empty($_POST['userfirstname']))
    {
        $data['userfirstname'] = strip_tags($_POST['userfirstname']);
    } else {
        $errors['userfirstname'] = 'Не указано имя!';
    }

    if (!empty($_POST['userlastname']))
    {
        $data['userlastname'] = strip_tags($_POST['userlastname']);
    } else {
        $errors['userlastname'] = 'Не указана фамилия!';
    }

    if (!empty($_POST['email']))
    {
        $data['email'] = strip_tags($_POST['email']);
    } else {
        $errors['email'] = 'Не указан e-mail!';
    }

    if (!empty($_POST['phone']))
    {
        $data['phone'] = strip_tags($_POST['phone']);
    } else {
        $errors['phone'] = 'Не указан телефон!';
    }

    $data['topic'] = $_POST['topic'];

    $data['payment'] = $_POST['payment'];
    
    if (!empty($_POST['mailing']))
    {
        $data['mailing'] = $_POST['mailing'];
    } else {
        $data['mailing'] = 0;
    }

    if ($errors) {
        $subjects = Subject::all();
        $payments = Payment::all();
        include 'templates/conf.php';
        exit();
    } else {
        $sql = Database::getInstance()->pdo()->prepare('INSERT INTO participants (username, lastname, email, phone, subject_id, payment_id, mailing, deleted_at, created_at, updated_at) VALUES (:username, :lastname, :email, :phone, :subject_id, :payment_id, :mailing, :deleted_at, :created_at, :updated_at);');
        $sql->execute([
            'username' => $data['userfirstname'],
            'lastname' => $data['userlastname'],
            'email' => $data['email'],
            'phone' => $data['phone'],
            'subject_id' => $data['topic'],
            'payment_id' => $data['payment'],
            'mailing' =>  $data['mailing'],
            'deleted_at' => null,
            'created_at' => date('Y-m-d h:i:s'),
            'updated_at' => date('Y-m-d h:i:s'),
        ]);
        header("Location: ../success_page/success.php");
        exit();

    }


    // exit();
} else {
    // Отображение пустой формы
    $subjects = Subject::all();
    $payments = Payment::all();
    $participants = Participant::all();

    $data = [
        'userfirstname' => '',
        'userlastname' => '',
        'email' => '',
        'phone' => '',
        'topic' => $_POST['topic'] ?? '',
        'payment' => $_POST['payment'] ?? '',
        'mailing' => '',
    ];

    include 'templates/conf.php';
}
