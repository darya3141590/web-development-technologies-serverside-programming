<?php

spl_autoload_register();

use App\Database;
use App\Models\Subject;
use App\Models\Payment;
use App\Models\Participant;

$subjects = Subject::all();
$payments = Payment::all();
$participants = Participant::all();

if (isset($_POST['delete'])) {
    foreach ($_POST['delete'] as $id) {
        $participant = Participant::find($id+1);
        if ($participant) {
            $participant->delete();
        }
    }
    header('Location: ' . $_SERVER['PHP_SELF']);
    exit();
}
include 'templates/admin.php';