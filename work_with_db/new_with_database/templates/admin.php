<?php

?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>ADMIN</title>
        <link rel='stylesheet' type='text/css' media='screen' href='../main.css'>
    </head>
    <body>
    <div class="main table">
    <h2>ADMIN</h2>
    <div class="main-agileinfo">
    <div class="content">
    <form method="POST" action="">

        <?php 
        $subjects = ['Бизнес и коммуникации', 'Технологии', 'Реклама', 'Маркетинг', 'Проектирование'];
        $payments = ['WebMoney', 'Яндекс.Деньги', 'PayPal', 'Кредитная карта', 'Робокасса'];
        echo '<table border="1">';
        echo '<thead><tr><th>ID</th><th>Username</th><th>Lastname</th><th>Email</th><th>Phone</th><th>Subject</th><th>Payment</th><th>Mailing</th><th>Delete</th></tr></thead>';
        foreach ($participants as $index => $participant)
        {
            if ($participant['deleted_at'] !== null) {
                continue;
            }
            echo '<tr>';
            echo '<td>' . $participant['id'] . '</td>';
            echo '<td>' . $participant['username'] . '</td>';
            echo '<td>' . $participant['lastname'] . '</td>';
            echo '<td>' . $participant['email'] . '</td>';
            echo '<td>' . $participant['phone'] . '</td>';
            echo '<td>' . $subjects[$participant['subject_id']-1] . '</td>';
            echo '<td>' . $payments[$participant['payment_id']-1] . '</td>';
            echo '<td>' . $participant['mailing'] . '</td>';?>
            <td><input type="checkbox" class="checkbox" name="delete[]" value="<?php echo $index; ?>"></td>
            <?php echo '</tr>';
        }
        echo '</table>';
        ?>
        <button type="submit">Delete selected</button>
    </form>
    </div>
    </div>
    </div>

    </body>
</html>
