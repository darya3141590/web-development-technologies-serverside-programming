CREATE TABLE `participants` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(255) NOT NULL,
  `lastname` VARCHAR(255) NOT NULL,
  `email` VARCHAR(255) NOT NULL,
  `phone` VARCHAR(255) NOT NULL,
  `subject_id` INT(10) NOT NULL,
  `payment_id` INT(10) NOT NULL,
  `mailing` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
  `deleted_at` TIMESTAMP NULL DEFAULT NULL,
  `created_at` TIMESTAMP NOT NULL,
  `updated_at` TIMESTAMP NOT NULL,
  PRIMARY KEY(`id`),
  INDEX `subject_id` (`subject_id`),
  INDEX `payment_id` (`payment_id`),
  INDEX `deleted_at` (`deleted_at`)
);