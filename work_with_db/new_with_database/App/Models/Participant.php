<?php

namespace App\Models;

use App\Database;
use PDO;

class Participant extends Model
{
    protected static $table = 'participants';

    public $id;
    public $username;
    public $lastname;
    public $email;
    public $phone;
    public $subject_id;
    public $payment_id;
    public $mailing;
    public $deleted_at;
    public $created_at;
    public $updated_at;

    public static function find($id)
    {
        $sql = Database::getInstance()->pdo()->prepare("SELECT * FROM " . static::$table . " WHERE id = :id");
        $sql->bindParam(':id', $id);
        $sql->execute();

        $result = $sql->fetch(PDO::FETCH_ASSOC);

        if ($result) {
            $participant = new static;
            $participant->id = $result['id'];
            $participant->username = $result['username'];
            $participant->lastname = $result['lastname'];
            $participant->email = $result['email'];
            $participant->phone = $result['phone'];
            $participant->subject_id = $result['subject_id'];
            $participant->payment_id = $result['payment_id'];
            $participant->mailing = $result['mailing'];
            $participant->deleted_at = $result['deleted_at'];
            $participant->created_at = $result['created_at'];
            $participant->updated_at = $result['updated_at'];

            return $participant;
        }

        return null;
    }
    public function delete()
    {
        $this->deleted_at = date('Y-m-d h:i:s');
        $this->updated_at = date('Y-m-d h:i:s');
        $this->save();
    }

    public function save()
    {
        $sql = Database::getInstance()->pdo()->prepare("UPDATE " . static::$table . " SET
        deleted_at = :deleted_at,
        updated_at = :updated_at
        WHERE id = :id;");
        $sql->bindParam(':deleted_at', $this->deleted_at);
        $sql->bindParam(':updated_at', $this->updated_at);
        $sql->bindParam(':id', $this->id);

        $sql->execute();
    }
}