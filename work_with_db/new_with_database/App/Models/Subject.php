<?php

namespace App\Models;

class Subject extends Model
{
    protected static $table = 'subjects';

    public $id;
    public $name;
}