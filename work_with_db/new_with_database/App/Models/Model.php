<?php

namespace App\Models;

use App\Database;

abstract class Model
{
    protected static $table;
    protected static $db;

    public static function pdo()
    {
        if (empty(static::$db)) {
            static::$db = Database::getInstance();
        }
        return static::$db->pdo();
    }

    public static function all()
    {
        $sql = static::pdo()->prepare('SELECT * FROM ' . static::$table . ';');
        $sql->execute();
        return $sql->fetchAll();
    }
}