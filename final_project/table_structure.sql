CREATE TABLE `durations` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  PRIMARY KEY(`id`)
);

INSERT INTO `durations` (`name`) VALUES
('Менее часа'),
('1 час'),
('2 часа'),
('3 часа'),
('Более 3 часов');

CREATE TABLE `types` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  PRIMARY KEY(`id`)
);

INSERT INTO `types` (`name`) VALUES
('встреча'),
('звонок'),
('совещание'),
('дело');

CREATE TABLE `tasks` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `subject` VARCHAR(255) NOT NULL,
  `type_id` INT(10) NOT NULL,
  `location` VARCHAR(255) NOT NULL,
  `date` DATETIME,
  `duration_id` INT(10) NOT NULL,
  `comment`VARCHAR(500),
  `done` TIMESTAMP NULL DEFAULT NULL,
  PRIMARY KEY(`id`),
  INDEX `type_id` (`type_id`),
  INDEX `duration_id` (`duration_id`),
  INDEX `done` (`done`)
);