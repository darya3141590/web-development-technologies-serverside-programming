<?php

spl_autoload_register();

use App\Database;
use App\Models\Duration;
use App\Models\Type;
use App\Models\Task;


// Проверяем, был ли выбран тип "Задачи на конкретную дату"
$showDateInput = ($_POST['task_type'] === 'specific_date');
$specificDate = $_POST['specific_date'] ?? '';
$durations_info = ['Менее часа', '1 час', '2 часа', '3 часа', 'Более 3 часов'];
$types_info = ['Встреча', 'Звонок', 'Совещание', 'Дело'];

$filteredTasks = [];

$filter = 'today';

if (isset($_GET['filter'])) {
    $filter = $_GET['filter'];
}

function showTasksByFilter($filter) {

    $today = date('Y-m-d');
    $tomorrow = date('Y-m-d', strtotime('+1 day'));
    $nextWeek = date('Y-m-d', strtotime('+1 week'));

    switch ($filter) {
        case 'today':
            $filteredTasks = Task::getTasksByToday($today);
            break;
        case 'tomorrow':
            $filteredTasks = Task::getTasksByTomorrow($tomorrow);
            break;
        default:
            return [];
    }
}


if ($_SERVER['REQUEST_METHOD'] === 'POST')
{
    if (isset($_POST['add'])) {
        $data = [
            'subject' => '',
            'types' => '',
            'location' => '',
            'date' => '',
            'time' => '',
            'durations' => '',
            'comment' => '',
        ];

        $errors = [];

        if (!empty($_POST['subject']))
        {
            $data['subject'] = strip_tags($_POST['subject']);
        } else {
            $errors['subject'] = 'Не указана тема!';
        }

        $data['types'] = $_POST['types'];

        if (!empty($_POST['location']))
        {
            $data['location'] = strip_tags($_POST['location']);
        } else {
            $data['location'] = '-';
        }

        if (!empty($_POST['date']) && (!empty($_POST['time'])))
        {
            $data['date'] = $_POST['date'];
            $data['time'] = $_POST['time'];
        } else {
            $errors['date'] = 'Не указана дата/время!';
        }

        $data['durations'] = $_POST['durations'];
        $data['comment'] = $_POST['comment'];


        if ($errors) {
            $durations = Duration::all();
            $types = Type::all();
            include 'templates/calendar.php';
            exit();
        } else {
            $sql = Database::getInstance()->pdo()->prepare('INSERT INTO tasks (subject, type_id, location, date, duration_id, comment, done) VALUES (:subject, :type_id, :location, :date, :duration_id, :comment, :done);');
            $sql->execute([
                'subject' => $data['subject'],
                'type_id' => $data['types'],
                'location' => $data['location'],
                'date' => $data['date'] . ' ' . $data['time'] . ':00',
                'duration_id' => $data['durations'],
                'comment' => $data['comment'],
                'done' => null,
            ]);

        }
    } elseif (isset($_POST['finish'])) {
        if (isset($_POST['done'])) {
            echo 'jtjjt';
            foreach ($_POST['done'] as $id) {
                $task = Task::find($id+1);
                if ($task) {
                    $task->done();
                }
            }
            header('Location: ' . $_SERVER['PHP_SELF']);
            exit();
        }
    } elseif (isset($_POST['filtr'])) {
        $taskType = $_POST['task_type'];

        switch ($taskType) {
            case 'current':
                $filteredTasks = Task::getTasksByType();
                break;
            case 'overdue':
                $filteredTasks = Task::getOverdueTasks();
                break;
            case 'completed':
                $filteredTasks = Task::getCompletedTasks();
                break;
            case 'specific_date':
                $specificDate = $_POST['specific_date'];
                $filteredTasks = Task::getTasksBySpecificDate($specificDate);
                break;
            default:
                // Обработка ошибки неверного типа задачи
                break;
        }
    }

} 
$durations = Duration::all();
$types = Type::all();
$tasks = Task::all();

showTasksByFilter($filter);

$data = [
    'subject' => '',
    'types' => $_POST['types']  ?? '',
    'location' => '',
    'date' => '',
    'time' => '',
    'durations' => $_POST['durations']  ?? '',
    'comment' => '',
];

include 'templates/calendar.php';
