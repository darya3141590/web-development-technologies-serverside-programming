<?php

use App\Models\Task;
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <title>Calendar</title>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <!-- <link rel='stylesheet' type='text/css' media='screen' href='main.css'> -->
  	<link rel='stylesheet' type='text/css' media='screen' href='calend.css'> 
</head>
<body>
<div class="main">
<h1>Мой календарь</h1>

<div class="main-agileinfo">
<div class="content">
<form method="post">
    <div>
        <label for="subject">Тема:</label>
        <input type="text" name="subject" value="<?=htmlspecialchars($_POST['subject'] ?? '') ?>">
        <span class="errors"><?= $errors['subject'] ?? '' ?></span>
        <br>
    </div>
    <div>
        <label for="types">Тип:</label>
        <select name="types">
        <?php foreach ($types as $type): ?>
            <option value="<?= htmlspecialchars($type['id']) ?>"><?= htmlspecialchars($type['name']) ?></option>
            <?php endforeach ?>
            <!-- <option value="встреча">Встреча</option>
            <option value="звонок">Звонок</option>
            <option value="совещание">Совещание</option>
            <option value="дело">Дело</option> -->
        </select>
    </div>
    <div>
        <label for="location">Место:</label>
        <input type="text" name="location" value="<?=htmlspecialchars($_POST['location'] ?? '') ?>">
        <span class="errors"><?= $errors['location'] ?? '' ?></span>
        <br>
    </div>
    <div>
        <label for="date">Дата и время:</label>
        <input type="date" name="date">
        <input type="time" name="time">
        <span class="errors"><?= $errors['date'] ?? '' ?></span>
    </div>
    <div>
        <label class="dur" for="durations">Длительность:</label>
        <select name="durations">
        <?php foreach ($durations as $duration): ?>
            <option value="<?= htmlspecialchars($duration['id']) ?>"><?= htmlspecialchars($duration['name']) ?></option>
            <?php endforeach ?>
        <!-- <option value="Менее часа">Менее часа</option>
            <option value="1 час">1 час</option>
            <option value="2 часа">2 часа</option>
            <option value="3 часа">3 часа</option>
            <option value="Более 3 часов">Более 3 часов</option> -->
        </select>
    </div>
    <div>
        <label for="comment">Комментарий:</label>
        <textarea rows="6" cols="30" name="comment"></textarea>
    </div>
    <button type="submit" name="add">Добавить</button>
</form>
 </div>
</div>
<div class="main-agileinfo">
<div class="content">
<form method="post" id="taskForm">
<div class="tsk">
        <select name="task_type" id="task_type">
            <option value="current">Текущие задачи</option>
            <option value="overdue">Просроченные задачи</option>
            <option value="completed">Выполненные задачи</option>
            <option value="specific_date">Задачи на конкретную дату</option>
        </select>
    </div>
    <div id="taskFilters">';
<a class="filterButton' . ($filter === 'today' ? ' active' : '') . '" href="?filter=today">Сегодня</a>
<a class="filterButton' . ($filter === 'tomorrow' ? ' active' : '') . '" href="?filter=tomorrow">Завтра</a>
<a class="filterButton' . ($filter === 'thisWeek' ? ' active' : '') . '" href="?filter=thisWeek">На эту неделю</a>
<a class="filterButton' . ($filter === 'nextWeek' ? ' active' : '') . '" href="?filter=nextWeek">На следующую неделю</a>
</div>';

    
    <div id="specific_date_container" style="display: <?php echo $showDateInput ? 'block' : 'none'; ?>">
        <label for="specific_date">Дата:</label>
        <input type="date" name="specific_date" id="specific_date">
    </div>
    <div id="taskTableContainer">
    <table border="1">
        <thead>
            <tr>
                <th>Тип</th>
                <th>Задача</th>
                <th>Место</th>
                <th>Дата и время</th>
                <th>Отметить</th>
            </tr>
        </thead>
        <tbody>
        <?php foreach ($filteredTasks as $index => $task): ?>
            <tr>
                <td><?= $types_info[$task->type_id-1] ?></td>
                <td><a href="edit_task.php?id=<?=  Task::findTaskIdByName($task->subject) ?>"><?= $task->subject ?></a></td>
                <td><?= $task->location ?></td>
                <td><?= $task->date ?></td>
                <td><input type="checkbox" class="checkbox" name="done[]" value="<?php echo $index; ?>"></td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
</div>
        <button type="submit" name="finish">Завершить</button>
        <button type="submit" name="filtr">Фильтровать</button>
</form>
</div>
</div>
<script>
    document.getElementById('task_type').addEventListener('change', function() {
        var container = document.getElementById('specific_date_container');
        var showDateInput = (this.value === 'specific_date');

        container.style.display = showDateInput ? 'block' : 'none';

    });
</script> 
</div>

</body>
</html>











