<?php

namespace App\Models;

use App\Database;
use PDO;

class Task extends Model
{
    protected static $table = 'tasks';

    public $id;
    public $subject;
    public $type_id;
    public $location;
    public $date;
    public $duration_id;
    public $comment;
    public $done;

    public static function find($id)
    {
        $sql = Database::getInstance()->pdo()->prepare("SELECT * FROM " . static::$table . " WHERE id = :id");
        $sql->bindParam(':id', $id);
        $sql->execute();

        $result = $sql->fetch(PDO::FETCH_ASSOC);

        if ($result) {
            $task = new static;
            $task->id = $result['id'];
            $task->subject = $result['subject'];
            $task->type_id = $result['type_id'];
            $task->location = $result['location'];
            $task->date = $result['date'];
            $task->duration_id = $result['duration_id'];
            $task->comment= $result['comment'];
            $task->done = $result['done'];

            return $task;
        }

        return null;
    }
    public static function findTaskIdByName($name)
    {
        $db = Database::getInstance()->pdo();
        $query = $db->prepare('SELECT id FROM tasks WHERE subject = :name');
        $query->bindParam(':name', $name);
        $query->execute();
        $taskId = $query->fetchColumn();

        return $taskId ? (int)$taskId : null;
    }
    public function done()
    {
        $this->done = date('Y-m-d h:i:s');
        $this->save();
    }

    public function save()
    {
        $sql = Database::getInstance()->pdo()->prepare("UPDATE " . static::$table . " SET
        done = :done
        WHERE id = :id;");
        $sql->bindParam(':done', $this->done);
        $sql->bindParam(':id', $this->id);

        $sql->execute();
    }
    public static function getTasksByType()
    {
        $db = Database::getInstance()->pdo();
        $query = $db->prepare("SELECT * FROM " . static::$table . " WHERE (date  > CURDATE()) AND (done IS NULL)");
        $query->execute();

        $tasks = [];

        while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
            $task = new static;
            $task->id = $row['id'];
            $task->subject = $row['subject'];
            $task->type_id = $row['type_id'];
            $task->location = $row['location'];
            $task->date = $row['date'];
            $task->duration_id = $row['duration_id'];
            $task->comment = $row['comment'];
            $task->done = $row['done'];

            $tasks[] = $task;
        }

        return $tasks;
    }
    public static function getTasksByToday($today)
    {
        $db = Database::getInstance()->pdo();
        $query = $db->prepare("SELECT * FROM " . static::$table . " WHERE DATE(date) = :today");
        $query->bindParam(':today', $today);
        $query->execute();

        $tasks = [];

        while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
            $task = new static;
            $task->id = $row['id'];
            $task->subject = $row['subject'];
            $task->type_id = $row['type_id'];
            $task->location = $row['location'];
            $task->date = $row['date'];
            $task->duration_id = $row['duration_id'];
            $task->comment = $row['comment'];
            $task->done = $row['done'];

            $tasks[] = $task;
            
        }

        return $tasks;
    }
    public static function getTasksByTomorrow($tomorrow)
    {
        $db = Database::getInstance()->pdo();
        $query = $db->prepare("SELECT * FROM " . static::$table . " WHERE DATE(date) = :tomorrow");
        $query->bindParam(':tomorrow', $tomorrow);
        $query->execute();

        $tasks = [];

        while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
            $task = new static;
            $task->id = $row['id'];
            $task->subject = $row['subject'];
            $task->type_id = $row['type_id'];
            $task->location = $row['location'];
            $task->date = $row['date'];
            $task->duration_id = $row['duration_id'];
            $task->comment = $row['comment'];
            $task->done = $row['done'];

            $tasks[] = $task;
            
        }

        return $tasks;
    }
    public static function getOverdueTasks()
    {
        $db = Database::getInstance()->pdo();
        $query = $db->prepare("SELECT * FROM " . static::$table . " WHERE (date < CURDATE()) AND (done IS NULL)");
        $query->execute();

        $tasks = [];

        while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
            $task = new static;
            $task->id = $row['id'];
            $task->subject = $row['subject'];
            $task->type_id = $row['type_id'];
            $task->location = $row['location'];
            $task->date = $row['date'];
            $task->duration_id = $row['duration_id'];
            $task->comment = $row['comment'];
            $task->done = $row['done'];

            $tasks[] = $task;
        }

        return $tasks;
    }
    public static function getCompletedTasks()
    {
        $db = Database::getInstance()->pdo();
        $query = $db->prepare("SELECT * FROM " . static::$table . " WHERE done IS NOT NULL");
        $query->execute();

        $tasks = [];

        while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
            $task = new static;
            $task->id = $row['id'];
            $task->subject = $row['subject'];
            $task->type_id = $row['type_id'];
            $task->location = $row['location'];
            $task->date = $row['date'];
            $task->duration_id = $row['duration_id'];
            $task->comment = $row['comment'];
            $task->done = $row['done'];

            $tasks[] = $task;
        }

        return $tasks;
    }
    public static function getTasksBySpecificDate($date)
    {
        $db = Database::getInstance()->pdo();
        $query = $db->prepare("SELECT * FROM " . static::$table . " WHERE DATE(date) = :date");
        $query->bindParam(':date', $date);
        $query->execute();

        $tasks = [];

        while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
            $task = new static;
            $task->id = $row['id'];
            $task->subject = $row['subject'];
            $task->type_id = $row['type_id'];
            $task->location = $row['location'];
            $task->date = $row['date'];
            $task->duration_id = $row['duration_id'];
            $task->comment = $row['comment'];
            $task->done = $row['done'];

            $tasks[] = $task;
        }

        return $tasks;
    }
    public function update()
    {
        $db = Database::getInstance()->pdo();
        $query = $db->prepare('UPDATE tasks SET subject = :subject, type_id = :type_id, location = :location, date = :date, duration_id = :duration_id, comment = :comment WHERE id = :id');
        $query->bindParam(':id', $this->id);
        $query->bindParam(':subject', $this->subject);
        $query->bindParam(':type_id', $this->type_id);
        $query->bindParam(':location', $this->location);
        $query->bindParam(':date', $this->date);
        $query->bindParam(':duration_id', $this->duration_id);
        $query->bindParam(':comment', $this->comment);
        $query->execute();
    }

}