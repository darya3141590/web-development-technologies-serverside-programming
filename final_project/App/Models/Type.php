<?php

namespace App\Models;

class Type extends Model
{
    protected static $table = 'types';

    public $id;
    public $name;
}