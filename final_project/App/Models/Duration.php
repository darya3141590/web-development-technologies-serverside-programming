<?php

namespace App\Models;

class Duration extends Model
{
    protected static $table = 'durations';

    public $id;
    public $name;
}