<?php

namespace App;

use PDO;

class Database
{
    protected static $instance;
    protected static $pdo;

    protected function __construct(){}

    public static function getInstance()
    {
        if (empty(static::$instance)) {
            static::$instance = new static;
        }
        return static::$instance;
    }

    public function pdo()
    {
        if (empty(static::$pdo)) {
            $dbConfig = include 'config/database.php';

            static::$pdo = new PDO('mysql:host=' . $dbConfig['dbhost'] . ';dbname='. $dbConfig['dbname'], $dbConfig['dbuser'], $dbConfig['dbpass']);
        }
        return static::$pdo;
    }
}