<?php
spl_autoload_register();

use App\Database;
use App\Models\Duration;
use App\Models\Type;
use App\Models\Task;

$taskId = $_GET['id'] ?? null;

if (!$taskId) {
    header('Location: calendar.php');
    exit();
}

$task = Task::find($taskId);

if (!$task) {
    header('Location: calendar.php');
    exit();
}

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $subject = $_POST['subject'] ?? '';
    $typeId = $_POST['types'] ?? '';
    $location = $_POST['location'] ?? '';
    $date = $_POST['date'] ?? '';
    $time = $_POST['time'] ?? '';
    $durationId = $_POST['durations'] ?? '';
    $comment = $_POST['comment'] ?? '';

    $errors = [];
    if (empty($subject)) {
        $errors['subject'] = 'Не указана тема!';
    }
    if (empty($date) || empty($time)) {
        $errors['date'] = 'Не указана дата/время!';
    }

    if (count($errors) === 0) {
        $dateTime = $date . ' ' . $time . ':00';
        $task->update([
            'subject' => $subject,
            'type_id' => $typeId,
            'location' => $location,
            'date' => $dateTime,
            'duration_id' => $durationId,
            'comment' => $comment,
        ]);

        header('Location: calendar.php');
        exit();
    }
}

$types = Type::all();
$durations = Duration::all();

?>

<!DOCTYPE html>
<html>
<head>
    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <title>Edit Task</title>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <link rel='stylesheet' type='text/css' media='screen' href='calend.css'>
</head>
<body>
<div class="main">
    <h1>Редактирование задачи</h1>

<div class="main-agileinfo">
<div class="content">
    <form method="post">
        <div>
            <label for="subject">Тема:</label>
            <input type="text" name="subject" value="<?= htmlspecialchars($task->subject) ?>">
            <span class="errors"><?= $errors['subject'] ?? '' ?></span>
        </div>
        <div>
            <label for="types">Тип:</label>
            <select name="types">
                <?php foreach ($types as $type): ?>
                    <option value="<?= htmlspecialchars($type['id']) ?>" <?= ($task->type_id === $type['id']) ? 'selected' : '' ?>>
                        <?= htmlspecialchars($type['name']) ?>
                    </option>
                <?php endforeach ?>
            </select>
        </div>
        <div>
            <label for="location">Место:</label>
            <input type="text" name="location" value="<?= htmlspecialchars($task->location) ?>">
        </div>
        <div>
            <label for="date">Дата и время:</label>
            <input type="date" name="date" value="<?= date('Y-m-d', strtotime($task->date)) ?>">
            <input type="time" name="time" value="<?= date('H:i', strtotime($task->date)) ?>">
            <span class="errors"><?= $errors['date'] ?? '' ?></span>
        </div>
        <div>
            <label for="durations">Длительность:</label>
            <select name="durations">
                <?php foreach ($durations as $duration): ?>
                    <option value="<?= htmlspecialchars($duration['id']) ?>" <?= ($task->duration_id === $duration['id']) ? 'selected' : '' ?>>
                        <?= htmlspecialchars($duration['name']) ?>
                    </option>
                <?php endforeach ?>
            </select>
        </div>
        <div>
            <label for="comment">Комментарий:</label>
            <textarea rows="10" cols="45" name="comment"><?= htmlspecialchars($task->comment) ?></textarea>
        </div>
        <button type="submit" name="update">Обновить</button>
    </form>
    </div>
</div>
</body>
</html>
