<?php

function extension($file)
{
    echo preg_replace("/.*?\./",'',$file);
}

function is_archive($file)
{
    $match = [];
    if (preg_match_all('/\.(?:z(?:ip|[0-9]{2})|r(?:ar|[0-9]{2})|jar|bz2|gz|tar|rpm)$/i', $file, $match)) { 
            echo 'Это архив'; 
    } else { 
        return 'Не архив'; 
    } 
}

function is_audiofile($file)
{
    $match = [];
    if (preg_match_all('/\.(?:mp3|wav|og(?:g|a)|flac|midi?|rm|aac|wma|mka|ape)$/i', $file, $match)) { 
            echo 'Это аудиофайл'; 
    } else { 
        return 'Не аудиофайл'; 
    } 
}

function is_videofile($file)
{
    $match = [];
    if (preg_match_all('/\.(?:mpeg|ra?m|avi|mp(?:g|e|4)|mov|divx|asf|qt|wmv|m\dv|rv|vob|asx|ogm)$/i', $file, $match)) { 
            echo 'Это видеофайл'; 
    } else { 
        return 'Не видеофайл'; 
    } 
}

function is_image($file)
{
    $match = [];
    if (preg_match_all('/\.(?:jp(?:e?g|e|2)|gif|png|tiff?|bmp|ico)$/i', $file, $match)) { 
            echo 'Это картинка'; 
    } else { 
        return 'Не картинка'; 
    } 
}

function title($html)
{
    if (preg_match_all('#<title[^>]*?>(.*?)</title>#si', $html, $matches)) { 
        foreach ($matches[1] as $val) { 
            echo $val . "<br>"; 
        } 
    } else { 
        return 'Такой строки нет'; 
    } 
}

function a_href($html) 
{ 
    if (preg_match_all('~<a [^<>]*href=["\']([^\'"]+)[\'"][^<>]*>~si', $html, $matches)) { 
        foreach ($matches[1] as $val) { 
            echo $val . "<br>"; 
        } 
    } else { 
        return 'Ссылок нет'; 
    } 
} 

function img_src($html) 
{ 
    if (preg_match_all('~<img [^<>]*src=["\']([^\'"]+)[\'"][^<>]*>~si', $html, $matches)) { 
        foreach ($matches[1] as $val) { 
            echo $val . "<br>"; 
        } 
    } else { 
    return 'Ссылок нет'; 
    } 
} 

function highlight($text, $string) 
{ 
    echo preg_replace("/" . $string . "/", "<strong>" . $string . "</strong>", $text); 
}

function smile($text) 
{ 
    echo preg_replace(['/:\)/', '/;\)/', '/:\(/'], ['<img src="smile.png" alt=":)">', '<img src="wink.png" alt=";)">', '<img src="sad.png" alt=":(">'], $text);  
} 

function space($string) 
{ 
    echo preg_replace('~\s+~', ' ', $string); 
} 

