<?php

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <title>Success page</title>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <link rel='stylesheet' type='text/css' media='screen' href='success.css'>
</head>
<body>
<p class="fig"><img src="../images/checkbox.png" 
   width="100" height="78" alt="Success"></p>
<h3>Congratulations! You are registered.</h3>
</body>
</html>