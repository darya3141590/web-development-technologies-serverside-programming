<?php

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <title>Main</title>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <link rel='stylesheet' type='text/css' media='screen' href='main.css'>
</head>
<body>
<div class="main">
<h1>Form</h1>

<!-- <?php if (!empty($errors)) : ?>
    <p>Обратите внимание на ошибки!</p>
<?php endif ?> -->
<div class="main-agileinfo">
<div class="content">
<form method="POST" action="">
    <input type="text" class="text" name="userfirstname" placeholder="Username" value="<?=htmlspecialchars($_POST['userfirstname'] ?? '') ?>">
    <span class="errors"><?= $errors['userfirstname'] ?? '' ?></span>
    <br>
    <input type="text" class="text" name="userlastname" placeholder="Lastname" value="<?=htmlspecialchars($_POST['userlastname'] ?? '') ?>">
    <span class="errors"><?= $errors['userlastname'] ?? '' ?></span>
    <br>
    <input type="email"  class="text" name="email" placeholder="Email" value="<?=htmlspecialchars($_POST['email'] ?? '') ?>">
    <span class="errors"><?= $errors['email'] ?? '' ?></span>
    <br>
    <input type="tel" class="text" name="phone" placeholder="Phone" value="<?=htmlspecialchars($_POST['phone'] ?? '') ?>">
    <span class="errors"><?= $errors['phone'] ?? '' ?></span>
    <br>
    <select name="topic" class="text">
      <option value=""><?=htmlspecialchars($_POST['topic'] ?? '-- Select Topic Of Interest--') ?></option>
      <option value="Business">Business</option>
      <option value="Technologies">Technologies</option>
      <option value="Advertising and Marketing">Advertising and Marketing</option>
    </select>
    <span class="errors"><?= $errors['topic'] ?? '' ?></span>
    <br>
    <select name="payment" class="text">
      <option value=""><?=htmlspecialchars($_POST['payment'] ?? '-- Select Payment Method --') ?></option>
      <option value="Webmoney">Webmoney</option>
      <option value="Yandex Money">Yandex Money</option>
      <option value="Paypal">Paypal</option>
      <option value="Credit Card">Credit Card</option>
    </select>
    <span class="errors"><?= $errors['payment'] ?? '' ?></span>
    <br>
    <div class="mail">
    <label class="anim">
    <input type="checkbox" class="text checkbox" name="mailing" value="yes"><span>Receive Mailing</span>
    </label>
    </div>
    <br> 
    <button type="submit">SEND</button>
</form>
</div>
</div>
</div>
</body>
</html>