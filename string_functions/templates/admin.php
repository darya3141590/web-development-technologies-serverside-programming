<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>ADMIN</title>
        <link rel='stylesheet' type='text/css' media='screen' href='main.css'>
    </head>
    <body>
    <div class="main">
    <h2>ADMIN</h2>
    <div class="main-agileinfo">
    <div class="content">
    <form method="POST" action="">

        <ul>
            <?php foreach ($items as $key => $item): ?>
                <div class="mail">
                <li class="anim"><input type="checkbox" class="checkbox list" name="checked[]" value="<?= $key ?>"> <span><?= nl2br($item) ?></span></li>
                </div>
            <?php endforeach ?>
        </ul>

        <button type="submit">Delete selected</button>
    </form>
    </div>
    </div>
    </div>

    </body>
</html>
