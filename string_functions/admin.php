<?php

$dataDir = 'data';
$fileWithList = 'del.txt';

if ($_POST)
{
    if (!empty($_POST['checked'])) {
        $fileNames = [];
        foreach ($_POST['checked'] as $key ) {
            $fileName = $dataDir . '/' . $key . '.txt';
            $fileNames[] = $fileName . "\n";
            // if (file_exists($fileName)) {
            //     unlink($fileName);
            // }
            // разблокировать если нужно чтобы заявки удалились из папки
        }
        file_put_contents($fileWithList, $fileNames);
    }
}

$items = [];

foreach (glob($dataDir . '/*.txt') as $fileName) {
    $fileContents = file_get_contents($fileName);
    $key = basename($fileName, '.txt');
    $items[$key] = $fileContents;
}

include 'templates/admin.php';