<?php

$dataDir = 'data';


function read_applications($fileName) {
    $rows = [];
    $file = fopen($fileName, 'r');
    while (($row = fgets($file)) !== false) {
        if ($row !== "\n") {
            $data = explode('||', trim($row));
            $deleted = ($data[count($data) - 1] === 'deleted'); // Проверяем, является ли заявка удаленной
            if (!$deleted) {
                $rows[] = array_slice($data, 0, -1); // Исключаем статус удаления
            }
        }
    }
    fclose($file);
    return $rows;
}

function update_file($fileName, $deletedApplications) {
    $file = fopen($fileName, 'r+');
    $contents = fread($file, filesize($fileName));
    fseek($file, 0);
    ftruncate($file, 0);
    $rows = explode("\n", $contents);
    foreach ($rows as $row) {
        $data = explode('||', trim($row));
        $removed = array_pop($data);
        if (in_array($data, $deletedApplications)) {
            $data[10] = 'deleted';
            fwrite($file, implode("||", $data) . "\n");
        } elseif ($data[0]!="") {
            fwrite($file, $row . "\n");
        }
    }
    fclose($file);

}

$fileName = $dataDir . '/' . 'data.txt';
$applications = read_applications($fileName);

if (isset($_POST['delete'])) {
    $deletedApplications = [];
    $deletedIndices = $_POST['delete'];
    foreach ($deletedIndices as $index) {
        $deletedApplications[] = $applications[$index];
    }
    // Обновляем файл с пометкой удаленных заявок
    update_file($fileName, $deletedApplications);
    header('Location: ' . $_SERVER['PHP_SELF']);
    exit();
}

include 'templates/admin.php';