<?php

if ($_POST)
{
    $data = [
        'userfirstname' => '',
        'userlastname' => '',
        'email' => '',
        'phone' => '',
        'topic' => '',
        'payment' => '',
        'mailing' => '',
    ];

    $errors = [];

    if (!empty($_POST['userfirstname']))
    {
        $data['userfirstname'] = strip_tags($_POST['userfirstname']);
    } else {
        $errors['userfirstname'] = 'Не указано имя!';
    }

    if (!empty($_POST['userlastname']))
    {
        $data['userlastname'] = strip_tags($_POST['userlastname']);
    } else {
        $errors['userlastname'] = 'Не указана фамилия!';
    }

    if (!empty($_POST['email']))
    {
        $data['email'] = strip_tags($_POST['email']);
    } else {
        $errors['email'] = 'Не указан e-mail!';
    }

    if (!empty($_POST['phone']))
    {
        $data['phone'] = strip_tags($_POST['phone']);
    } else {
        $errors['phone'] = 'Не указан телефон!';
    }

    if (!empty($_POST['topic']))
    {
        $data['topic'] = $_POST['topic'];
    } else {
        $errors['topic'] = 'Не выбрана тема!';
    }
    if (!empty($_POST['payment']))
    {
        $data['payment'] = $_POST['payment'];
    } else {
        $errors['payment'] = 'Не указан платеж!';
    }
    if (!empty($_POST['mailing']))
    {
        $data['mailing'] = $_POST['mailing'];
    } else {
        $data['mailing'] = 'no';
    }

    if ($errors) {
        include '../templates/form.php';
    } else {

        $dataDir = 'data';

        if (!is_dir($dataDir)) {
            mkdir($dataDir, 0777, true);
        }

        $fileName = "data.txt";

        file_put_contents($dataDir . '/' . $fileName,
        uniqid()
        . '||' . time()
        . '||' . getenv('REMOTE_ADDR')
        . '||' . $data['userfirstname']
        . '||' . $data['userlastname']
        . '||' . $data['email']
        . '||' . $data['phone']
        . '||' . $data['topic']
        . '||' . $data['payment']
        . '||' . $data['mailing']
        . '||' . 'ok' . "\n", FILE_APPEND);
        header("Location: ../success_page/success.php");
        exit();
    }
}
else {
    include '../templates/form.php';
}