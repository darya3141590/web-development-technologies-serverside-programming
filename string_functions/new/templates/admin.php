<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>ADMIN</title>
        <link rel='stylesheet' type='text/css' media='screen' href='../main.css'>
    </head>
    <body>
    <div class="main table">
    <h2>ADMIN</h2>
    <div class="main-agileinfo">
    <div class="content">
    <form method="POST" action="">
        <table>
            <tr>
                <th>id</th>
                <th>time</th>
                <th>ip</th>
                <th>username</th>
                <th>lastname</th>
                <th>e-mail</th>
                <th>phone</th>
                <th>topic</th>
                <th>payment</th>
                <th>mailing</th>
                <th>Удалить</th>
            </tr>
            <?php foreach ($applications as $index => $application) {
                if (!in_array('deleted', $application)) { ?>
        <tr>
            <?php foreach ($application as $data) { ?>
                <td><?php echo $data; ?></td>
            <?php } ?>
            <td>
                <input type="checkbox" class="checkbox" name="delete[]" value="<?php echo $index; ?>">
            </td>
        </tr>
            <?php } ?>
    <?php } ?>
</table>

        <button type="submit">Delete selected</button>
    </form>
    </div>
    </div>
    </div>

    </body>
</html>
